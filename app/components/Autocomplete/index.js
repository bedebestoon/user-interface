/**
 *
 * Autocomplete
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import MaterialAutocomplete from '@material-ui/lab/Autocomplete';

function Autocomplete(props) {
  const {
    id,
    label,
    variant,
    options,
    isLoading,
    onSelect,
    getOptionLabel,
    className,
    inputClassName,
    ...others
  } = props;

  return (
    <MaterialAutocomplete
      // disabled
      loading={isLoading || !options || !options.length}
      id={id}
      options={Array.isArray(options) ? options : []}
      getOptionLabel={getOptionLabel}
      className={className}
      renderInput={params => (
        <TextField
          {...params}
          label={label}
          variant={variant}
          className={inputClassName}
        />
      )}
      onChange={(e, v) => onSelect(v)}
      {...others}
    />
  );
}

Autocomplete.propTypes = {
  id: PropTypes.string,
  options: PropTypes.any,
  onSelect: PropTypes.func,
  getOptionLabel: PropTypes.func,
  variant: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  className: PropTypes.string,
  inputClassName: PropTypes.string,
  isLoading: PropTypes.bool,
};

Autocomplete.defaultProps = {
  id: 'autocomplete',
  options: [],
  label: '',
  variant: 'outlined',
  getOptionLabel: option =>
    option.normalizedName ? option.normalizedName : '',
  onSelect: v => v,
};

export default memo(Autocomplete);
