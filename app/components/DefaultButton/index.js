/* eslint-disable prettier/prettier */
/* eslint-disable no-nested-ternary */
/**
 *
 * DefaultButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';

import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/ErrorOutline';

import useStyles from './styles';

import {
  XHR_IDLE,
} from '../../config/xhrStatuses';

import useStatusListener from '../../hooks/useStatusListener';

function DefaultButton(props) {
  const {
    status: _status,
    children,
    disabled,
    size,
    color,
    fontSize = "inherit",
    variant = "contained",
    defaultIcon = null,
    iconSize = "22px",
    iconMainColor = "default",
    iconErrorColor = "default",
    iconButton,
    gutter,
    ...others
  } = props;

  const {
    isFulfilled,
    isLoading,
    isRejected
  } = useStatusListener(_status)


  const buttonMainColor =
    iconMainColor === 'inherit' ? 'secondary' : iconMainColor;
  const buttonErrorColor =
    iconErrorColor === 'inherit' ? 'error' : iconErrorColor;

  const isDisabled = isLoading || disabled;

  return iconButton ? (
    <IconButton
      {...others}
      color={color}
      variant={variant}
      disabled={isDisabled}
      size={size}
    >
      {isFulfilled ? (
        <CheckIcon
          color={buttonMainColor}
          style={{ width: iconSize, height: iconSize }}
        />
      ) : isLoading ? (
        <CircularProgress color={buttonMainColor} size={iconSize} />
      ) : isRejected ? (
        <ErrorIcon
          color={buttonErrorColor}
          style={{ width: iconSize, height: iconSize }}
        />
      ) : (
        children
      )}
    </IconButton>
  ) : (
    <Button
      {...others}
      color={color}
      variant={variant}
      disabled={isDisabled}
      style={{ fontSize }}
      size={size}
      endIcon={
        isFulfilled ? (
          <CheckIcon
            color={iconMainColor}
            style={{ width: iconSize, height: iconSize }}
          />
        ) : isLoading ? (
          <CircularProgress color={iconMainColor} size={iconSize} />
        ) : isRejected ? (
          <ErrorIcon
            color={iconErrorColor}
            style={{ width: iconSize, height: iconSize }}
          />
        ) : (
          defaultIcon
        )
      }
    >
      {children}
    </Button>
  );
}

DefaultButton.propTypes = {
  status: PropTypes.string,
  children: PropTypes.any,
  classes: PropTypes.string,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  color: PropTypes.string,
  fontSize: PropTypes.string,
  variant: PropTypes.string,
  defaultIcon: PropTypes.any,
  iconSize: PropTypes.string,
  iconMainColor: PropTypes.string,
  iconErrorColor: PropTypes.string,
  iconButton: PropTypes.any,
  className: PropTypes.any,
  gutter: PropTypes.arrayOf([null, 'top', 'bottom'])
};

DefaultButton.defaultProps = {
  status: XHR_IDLE,
  fontSize: "inherit",
  variant: "contained",
  defaultIcon: null,
  iconSize: "22px",
  iconMainColor: "default",
  iconErrorColor: "default",
};

export default DefaultButton;
