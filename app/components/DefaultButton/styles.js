/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  gutterTop: {
    marginTop: theme.spacing(2)
  },
  gutterBottom: {
    marginBottom: theme.spacing(2)
  }
}));