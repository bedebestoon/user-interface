/**
 *
 * DefaultTextField
 *
 */

import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

import useStyles from './styles';

function DefaultTextField(props) {
  const {
    className,
    style,
    startAdornment,
    endAdornment,
    InputProps = {},
    showHelper,
    FormHelperTextProps = {},
    ...others
  } = props;

  const classes = useStyles();

  return (
    <TextField
      className={clsx(classes.root, className)}
      style={{
        ...style,
      }}
      InputProps={{
        ...InputProps,
        className: clsx(classes.inputContainer, InputProps.className),
        startAdornment: (
          <InputAdornment position="start" className={classes.adornment}>
            {Boolean(startAdornment) && startAdornment}
          </InputAdornment>
        ),
        endAdornment: (
          <InputAdornment position="end" className={classes.adornment}>
            {Boolean(endAdornment) && endAdornment}
          </InputAdornment>
        ),
      }}
      FormHelperTextProps={{
        ...FormHelperTextProps,
        className: clsx(
          classes.helper,
          { show: showHelper },
          FormHelperTextProps.className,
        ),
      }}
      {...others}
    />
  );
}

DefaultTextField.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  startAdornment: PropTypes.node,
  endAdornment: PropTypes.node,
  InputProps: PropTypes.object,
  showHelper: PropTypes.bool,
  FormHelperTextProps: PropTypes.object,
};

export default DefaultTextField;
