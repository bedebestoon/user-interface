/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {

  },
  adornment: {
    padding: `0 ${theme.spacing(1)}px`,
    color: theme.palette.grey[400],
    'img': {
      width: theme.spacing(3)
    }
  },
  helper: {
    transition: 'opacity 0.5s',
    opacity: 0,
    color: theme.palette.primary,
    '&.show': {
      opacity: 1,
    }
  }
}));