/* eslint-disable prettier/prettier */
import { useState } from 'react';
import apiHandler from 'utils/apiHandler';

import { UPLOAD_IMAGE_API } from 'config/api';
import * as STATUSES from 'config/xhrStatuses';

export default function () {
  const [status, setStatus] = useState(STATUSES.XHR_IDLE);
  const [url, setUrl] = useState(null);

  const uploadImage = image => {
    setStatus(STATUSES.XHR_PENDING);
    setUrl(null)
    apiHandler(UPLOAD_IMAGE_API, image)
      .then(res => {
        setUrl(res);
        setStatus(STATUSES.XHR_FULFILLED);
        return res;
      })
      .catch(() => setStatus(STATUSES.XHR_REJECTED));
  };

  return {
    uploadImage,
    status,
    url,
  }
}
