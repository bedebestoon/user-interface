/* eslint-disable prettier/prettier */
/**
 *
 * DefaultUploadButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import AttachFileIcon from '@material-ui/icons/AttachFile';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import useStatusListener from '../../hooks/useStatusListener';

import Button from '../DefaultButton';

import useStyles from './styles';

import useUpload from './hooks/useUpload';

function DefaultUploadButton(props) {
  const classes = useStyles();
  const {
    label = <FormattedMessage {...messages.submit} />,
    variant = "text",
    size = "medium",

    onUpload,
    onReady,
    onError,
  } = props;

  const { uploadImage, status, url } = useUpload();

  const handleFile = event => {
    const file = event.target.files[0] || null;
    if (file) {
      const formData = new FormData();
      formData.append('file', file)
      if (onReady) {
        onReady(formData);
      } else if (onUpload) {
        uploadImage(formData);
      }
    } else if (onError) onError();
  };

  function handleUploadDone() {
    if (onUpload) {
      onUpload(url)
    }
  }

  useStatusListener(status, handleUploadDone, null, true);

  return (
    <>
      <input
        accept="image"
        className={classes.input}
        id="file-upload-button"
        type="file"
        onChange={handleFile}
      />
      <label htmlFor="file-upload-button">
        <Button
          color="inherit"
          size={size}
          aria-label="file-upload-button"
          component="span"
          startIcon={<AttachFileIcon />}
          variant={variant}
          fullWidth
          status={status}
        >
          {label}
        </Button>
      </label>
    </>
  );
}

DefaultUploadButton.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  variant: PropTypes.string,
  size: PropTypes.string,
  iconButton: PropTypes.any,

  onUpload: PropTypes.func,
  onReady: PropTypes.func,
  onError: PropTypes.func,
};

export default DefaultUploadButton;
