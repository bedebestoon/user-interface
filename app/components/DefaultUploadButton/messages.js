/*
 * DefaultUploadButton Messages
 *
 * This contains all the text for the DefaultUploadButton component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DefaultUploadButton';

export default defineMessages({
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'آپلود',
  },
});
