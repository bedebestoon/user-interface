import FacebookIcon from '@material-ui/icons/Facebook';
import PhoneIcon from '@material-ui/icons/Phone';
import WebsiteIcon from '@material-ui/icons/Language';
import InstagramIcon from '@material-ui/icons/Instagram';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TwitterIcon from '@material-ui/icons/Twitter';
import PinterestIcon from '@material-ui/icons/Pinterest';
import YouTubeIcon from '@material-ui/icons/YouTube';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import PersonIcon from '@material-ui/icons/Person';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';

export function getIconSrc(name) {
  switch (name) {
    case 'instagram':
      return InstagramIcon;
    case 'phone':
      return PhoneIcon;
    case 'facebook':
      return FacebookIcon;
    case 'email':
      return MailOutlineIcon;
    case 'website':
      return WebsiteIcon;
    case 'twitter':
      return TwitterIcon;
    case 'youtube':
      return YouTubeIcon;
    case 'pinterest':
      return PinterestIcon;
    case 'linkedin':
      return LinkedInIcon;
    case 'person':
      return PersonIcon;
    case 'mobile':
      return PhoneAndroidIcon;
    default:
      return LiveHelpIcon;
  }
}

export const SOCIAL_NETWORKS_MATERIAL_ICONS = [
  {
    name: 'person',
    icon: PersonIcon,
  },
  {
    name: 'phone',
    icon: PhoneIcon,
  },
  {
    name: 'email',
    icon: MailOutlineIcon,
  },
  {
    name: 'facebook',
    icon: FacebookIcon,
  },
  {
    name: 'website',
    icon: WebsiteIcon,
  },
  {
    name: 'instagram',
    icon: InstagramIcon,
  },
  {
    name: 'twitter',
    icon: TwitterIcon,
  },
  {
    name: 'pinterest',
    icon: PinterestIcon,
  },
  {
    name: 'youtube',
    icon: YouTubeIcon,
  },
  {
    name: 'linkedin',
    icon: LinkedInIcon,
  },
  {
    name: 'mobile',
    icon: PhoneAndroidIcon,
  },
]