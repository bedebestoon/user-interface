/**
 *
 * ImageCarousel
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Flicking from '@egjs/react-flicking';
import { Fade } from '@egjs/flicking-plugins';

import useStyles from './styles';

function ImageCarousel(props) {
  const {
    data,
    showPlayButton,
    showThumbnails,
    classNames: {
      container: containerClassName,
      root: rootClassName,
      panel: panelClassName,
      image: imageClassName,
    },
    ...rest
  } = props;

  const [items, setItems] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    if (data.length) {
      handleItems();
    } else {
      setItems([]);
    }
  }, [data]);

  function handleItems() {
    const withPanel = data.map(item => (
      <div className={clsx(panelClassName, classes.panel)}>
        <img
          src={item}
          alt="product"
          className={clsx(imageClassName, classes.image)}
        />
      </div>
    ));
    setItems(withPanel);
  }

  const plugins = [new Fade()];

  return (
    <div className={clsx(containerClassName, classes.container)}>
      <Flicking
        className={clsx(rootClassName, classes.root)}
        tag="div"
        moveType={{ type: 'snap', count: 1 }}
        circular
        gap={10}
        plugins={plugins}
        collectStatistics={false}
        zIndex={2000}
        defaultIndex={1}
        {...rest}
      >
        {items}
      </Flicking>
    </div>
  );
}

ImageCarousel.propTypes = {
  data: PropTypes.any,
  showPlayButton: PropTypes.bool,
  showThumbnails: PropTypes.bool,
  classNames: PropTypes.shape({
    root: PropTypes.any,
    container: PropTypes.any,
    panel: PropTypes.any,
    image: PropTypes.any,
  }),
};

ImageCarousel.defaultProps = {
  data: [],
  showPlayButton: false,
  showThumbnails: false,
  classNames: {
    root: null,
    container: null,
    panel: null,
    image: null,
  },
};

export default ImageCarousel;
