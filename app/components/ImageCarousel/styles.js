/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  container:{
    height: 200,
    width: '100%'
  },
  root: {
    width: '100%',
    height: 200,
    margin: '0px auto',
    background: 'rgba(55, 55, 55 0.1)',
    borderRadius: '5px',
  },
  panel: {
    position: 'relative',
    display: 'block',
    // borderRadius: theme.shape.borderRadius,
    width: '100%',
    marginTop: theme.spacing(1),
    height: 190,
    textAlign: 'center',
    lineHeight: 200,
    overflow: 'hidden',

    '& :after':{
      content: '',
      position: 'absolute',
      width: '100%',
      height: '100%',
      top: 0,
      left: 0,
      background: 'rgba(0, 0, 0, 0.1)',
    }
  },
  image: {
    width: 'auto',
    height: '97%',
    top: '-100%',
    bottom: '-100%',
    margin: 'auto',
    position: 'absolute',
    left: '50%',
    transform: 'translate(-50%)',
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[24]
  }
}));
