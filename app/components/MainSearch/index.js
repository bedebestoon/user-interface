/**
 *
 * MainSearch
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import useStyles from './styles';

import Autocomplete from '../Autocomplete';
import Button from '../DefaultButton';

import useSticky from '../../hooks/useSticky';

const defaultOptions = {
  tradeG1: '',
  tradeG2: '',
};

function MainSearch(props) {
  const {
    actions: { handleGetTradeGroups, handleSearchTrades },
    state: {
      tradeGroups: {
        data: tradeGroupsData,
        // status: tradeGroupsStatus,
        isLoading: tradeGroupsIsLoading,
      },
      search: { status: searchStatus },
    },
  } = props;

  const [searchOptions, setSearchOptions] = useState(defaultOptions);
  useEffect(() => {
    const t = setTimeout(() => {
      handleGetTradeGroups();
    }, 500);
    return () => clearTimeout(t);
  }, []);

  const setOptions = o =>
    setSearchOptions(prev => ({
      ...prev,
      ...o,
    }));

  const { isSticky, element } = useSticky(300);

  const classes = useStyles();

  function handleSearch() {
    handleSearchTrades(searchOptions);
  }

  return (
    <div className={clsx(classes.root, { [classes.rootSticky]: isSticky })}>
      <Grid
        container
        spacing={1}
        ref={element}
        className={clsx(classes.innerContainer, {
          [classes.innerContainerSticky]: isSticky,
        })}
      >
        <Grid item md={isSticky ? 2 : 12}>
          <Typography
            variant="h6"
            className={clsx({ [classes.text]: !isSticky })}
          >
            <FormattedMessage {...messages.tradeBy} />
          </Typography>
        </Grid>
        <Grid item md={isSticky ? 3 : 12}>
          <Autocomplete
            id="trade-groups-1"
            value={searchOptions.tradeG1}
            onSelect={v => setOptions({ tradeG1: v })}
            options={tradeGroupsData}
            fullWidth
            isLoading={tradeGroupsIsLoading}
          />
        </Grid>
        <Grid item md={isSticky ? 1 : 12}>
          <Typography
            variant="h6"
            className={clsx({ [classes.text]: !isSticky })}
          >
            <FormattedMessage {...messages.tradeWith} />
          </Typography>
        </Grid>
        <Grid item md={isSticky ? 3 : 12}>
          <Autocomplete
            id="trade-groups-2"
            value={searchOptions.tradeG2}
            onSelect={v => setOptions({ tradeG2: v })}
            options={tradeGroupsData}
            fullWidth
            isLoading={tradeGroupsIsLoading}
          />
        </Grid>
        <Grid item container md={isSticky ? 2 : 12}>
          <Button
            className={isSticky ? classes.buttonSticky : classes.button}
            fullWidth={!isSticky}
            onClick={handleSearch}
            status={searchStatus}
          >
            <FormattedMessage {...messages.search} />
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

MainSearch.propTypes = {
  containerRef: PropTypes.any,
  state: PropTypes.object,
  actions: PropTypes.object,
};

MainSearch.defaultProps = {};

export default MainSearch;
