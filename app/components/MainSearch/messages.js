/*
 * MainSearch Messages
 *
 * This contains all the text for the MainSearch component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.MainSearch';

export default defineMessages({
  tradeBy: {
    id: `${scope}.tradeBy`,
    defaultMessage: 'معاوضه‌ی',
  },
  tradeWith: {
    id: `${scope}.tradeWith`,
    defaultMessage: 'با',
  },
  search: {
    id: `${scope}.search`,
    defaultMessage: 'جستجو',
  },
});
