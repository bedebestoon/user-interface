/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: `calc(100vh - ${theme.spacing(8)}px)`,
    width: '100%',
  },
  rootSticky:{
    height: `calc(100vh - ${theme.spacing(8)}px)`,
  },
  innerContainer: {
    display: 'flex',
    flex: 1,
  },
  innerContainerSticky: {
    height: 'auto',
    position: 'fixed',
    top: `calc(${theme.spacing(9)}px - 2px)`,
    width: '100vw',
    justifyContent: 'center',
    alignItems: 'center',
    padding: `${theme.spacing(1)}px 0`,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[2],

    '& div':{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
  },
  text: {
    // margin: theme.spacing(2)
  },
  button: {
    marginTop: '1rem',
  },
  buttonSticky: {
    justifySelf: 'center',

  }
}));