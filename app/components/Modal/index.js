/**
 *
 * Modal
 *
 */

import React, { useState, useRef, useEffect, createRef } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Dialog from '@material-ui/core/Dialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import ScrollableContainer from '../ScrollableContainer';

import useStyles from './styles';

function Modal(props) {
  const { className, style, onClose, childProps, open, ...others } = props;

  const [ref, setRef] = useState();

  const containerRef = createRef(null);

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    setRef(containerRef.current);
  });

  const classes = useStyles();

  return (
    <Dialog
      open={open}
      scroll="paper"
      className={clsx(classes.root, className)}
      style={{
        ...style,
      }}
      PaperProps={{
        className: classes.paper,
      }}
      fullWidth
      fullScreen={fullScreen}
      onClose={onClose}
      {...others}
    >
      <ScrollableContainer
        className={classes.container}
        forwardRef={containerRef}
      >
        {React.cloneElement(props.children, {
          container: ref,
          ...childProps,
        })}
      </ScrollableContainer>
    </Dialog>
  );
}

Modal.propTypes = {
  open: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  childProps: PropTypes.object,
  onClose: PropTypes.func,
  children: PropTypes.node,
};

export default Modal;
