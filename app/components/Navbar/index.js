/**
 *
 * Navbar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';

import messages from './messages';
import useStyles from './styles';

function Navbar(props) {
  const { isLoggedIn, onClickLogin, onClickRegisterTrade } = props;

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="inherit" variant="outlined" elevation={2}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <FormattedMessage {...messages.header} />
          </Typography>
          {isLoggedIn && (
            <Button
              color="secondary"
              variant="outlined"
              onClick={onClickRegisterTrade}
              className={classes.button}
            >
              <FormattedMessage {...messages.registerTrade} />
            </Button>
          )}
          <Button
            color="inherit"
            onClick={onClickLogin}
            className={classes.button}
          >
            {isLoggedIn ? (
              <FormattedMessage {...messages.logout} />
            ) : (
              <FormattedMessage {...messages.login} />
            )}
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

Navbar.propTypes = {
  isLoggedIn: PropTypes.object,
  onClickLogin: PropTypes.func,
  onClickRegisterTrade: PropTypes.func,
};

export default Navbar;
