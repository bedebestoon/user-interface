/**
 *
 * RegisterTradeForm
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';

import { useForm } from 'react-hook-form';

import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import TextField from '../DefaultTextField';
import Button from '../DefaultButton';
import Autocomplete from '../Autocomplete';
import UploadButton from '../DefaultUploadButton';
import ImageCarousel from '../ImageCarousel';

import useStyles from './styles';

function RegisterTradeForm(props) {
  const {
    state: {
      registerTrade: { status: registerTradeStatus },
      tradeGroups: { data: tradeGroupsData, isLoading: tradeGroupsIsLoading },
      user: { data: userData },
    },
    actions: { handleRegisterTrade },
  } = props;

  const [images, setImages] = useState([]);
  const [g1, setG1] = useState(null);
  const [g2, setG2] = useState(null);

  const { register, handleSubmit } = useForm();

  const classes = useStyles();

  function onSubmit(values) {
    if (handleRegisterTrade) {
      const tradeObject = {
        images,
        // eslint-disable-next-line no-underscore-dangle
        owner: userData.info._id,
        giving: {
          // eslint-disable-next-line no-underscore-dangle
          category: g1,
          fullName: values.tradeP1Name,
        },
        receiving: {
          // eslint-disable-next-line no-underscore-dangle
          category: g2,
          fullName: values.tradeP2Name,
          desc: values.memo,
        },
      };
      handleRegisterTrade(tradeObject);
    }
  }

  function handleImages(image) {
    if (!images.includes(image)) {
      const tmpImages = [...images, image];
      setImages(tmpImages);
    }
  }
  function NoImageAlert() {
    return (
      <div className={classes.noImageContainer}>
        <AddPhotoAlternateIcon className={classes.fadeIcon} />
        <Typography>
          <FormattedMessage {...messages.noImageMessage} />
        </Typography>
      </div>
    );
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          <FormattedMessage {...messages.header} />
        </Typography>
        <form
          className={classes.form}
          onSubmit={(e => e.preventDefault(), handleSubmit(onSubmit))}
        >
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Autocomplete
                name="tradeG1"
                variant="outlined"
                required
                fullWidth
                id="category"
                label={<FormattedMessage {...messages.category} />}
                autoFocus
                options={tradeGroupsData}
                // eslint-disable-next-line no-unused-expressions
                onSelect={v => setG1(v)}
                isLoading={tradeGroupsIsLoading}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="tradeP1Name"
                label={<FormattedMessage {...messages.tradeProductName} />}
                name="tradeP1Name"
                margin="dense"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                name="memo"
                label={<FormattedMessage {...messages.memo} />}
                type="memo"
                id="memo"
                multiline
                margin="dense"
                rows={2}
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} className={classes.imageContainer}>
              {images.length ? (
                <ImageCarousel
                  data={images}
                  classNames={{
                    root: classes.imageCarouselRoot,
                    panel: classes.imageCarouselPanel,
                    container: classes.imageCarouselContainer,
                  }}
                />
              ) : (
                  <NoImageAlert />
                )}
            </Grid>
            <Grid item xs={12} sm={12}>
              <UploadButton
                label={<FormattedMessage {...messages.uploadImageLabel} />}
                variant="contained"
                onUpload={handleImages}
                size="small"
              />
            </Grid>
          </Grid>
          <Grid container className={classes.tradeWithContainer} spacing={1}>
            <Typography component="h6" variant="h6">
              <FormattedMessage {...messages.tradeWith} />
            </Typography>
            <Grid item xs={12}>
              <Autocomplete
                name="category"
                variant="outlined"
                required
                fullWidth
                id="category"
                label={<FormattedMessage {...messages.category} />}
                autoFocus
                options={tradeGroupsData}
                onSelect={v => setG2(v)}
                isLoading={tradeGroupsIsLoading}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="tradeP2Name"
                name="tradeP2Name"
                label={<FormattedMessage {...messages.tradeProductName} />}
                inputRef={register}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            status={registerTradeStatus}
          >
            <FormattedMessage {...messages.submit} />
          </Button>
        </form>
      </div>
    </Container>
  );
}

RegisterTradeForm.propTypes = {
  state: PropTypes.object,
  actions: PropTypes.object,
};

export default memo(RegisterTradeForm);
