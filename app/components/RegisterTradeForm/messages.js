/*
 * RegisterTradeForm Messages
 *
 * This contains all the text for the RegisterTradeForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RegisterTradeForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'ثبت معاوضه‌ی جدید',
  },
  category: {
    id: `${scope}.category`,
    defaultMessage: 'دسته بندی',
  },
  tradeProductName: {
    id: `${scope}.tradeProductName`,
    defaultMessage: 'نام کامل محصول',
  },
  memo: {
    id: `${scope}.memo`,
    defaultMessage: 'توضیحات',
  },
  province: {
    id: `${scope}.province`,
    defaultMessage: 'This is the RegisterTradeForm component!',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'This is the RegisterTradeForm component!',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'تایید',
  },
  uploadImageLabel: {
    id: `${scope}.uploadImageLabel`,
    defaultMessage: 'بارگزاری تصویر',
  },
  noImageMessage: {
    id: `${scope}.uploadImageLabel`,
    defaultMessage: 'تصویر جدیدی بارگزاری کنید',
  },
  tradeWith: {
    id: `${scope}.tradeWith`,
    defaultMessage: 'معاوضه با',
  }
});
