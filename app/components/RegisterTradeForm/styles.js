/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  paper: {
    margin: theme.spacing(2,0,2,0),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(2, 0, 2, 0),
  },
  link: {
    direction: 'ltr !important'
  },
  imageContainer: {
    display: 'flex',
    padding: '0 !important',
    margin: theme.spacing(.5),
    height: 100,
    boxShadow: theme.shadows[1],
    backgroundColor: theme.palette.grey[100],
    borderRadius: theme.shape.borderRadius
  },
  noImageContainer:{
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageCarouselRoot: {
    height: '100px !important'
  },
  imageCarouselPanel:{
    height: '100px !important',
    margin: '0 !important',
  },
  imageCarouselContainer:{
    height: '100px !important'
  },
  tradeWithContainer:{
    marginTop: theme.spacing(1)
  }
}));
