/**
 *
 * TradeForm
 *
 */

import React, { memo, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Modal from '../Modal';
import RegisterTradeForm from '../RegisterTradeForm';

import useQueryString from '../../hooks/useQueryString';

function RegisterTradeFormModal(props) {
  const { location, history, user } = props;
  const [open, setOpen] = useState(false);

  const query = useQueryString(location.search);

  useEffect(() => {
    if (user) {
      if (query.registerTrade) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    } else {
      setOpen(false);
    }
  }, [query.registerTrade]);

  function handleCloseModal() {
    history.push(location.pathName);
  }

  return (
    <Modal open={open} onClose={handleCloseModal}>
      <RegisterTradeForm {...props} />
    </Modal>
  );
}

RegisterTradeFormModal.propTypes = {
  location: PropTypes.any,
  history: PropTypes.any,
};

export default withRouter(memo(RegisterTradeFormModal));
