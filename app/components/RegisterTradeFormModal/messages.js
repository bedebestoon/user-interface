/*
 * RegisterTradeFormModal Messages
 *
 * This contains all the text for the RegisterTradeFormModal component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RegisterTradeFormModal';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RegisterTradeFormModal component!',
  },
});
