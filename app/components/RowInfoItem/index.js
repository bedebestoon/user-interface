/**
 *
 * RowInfoItem
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import useStyles from './styles';

function RowInfoItem(props) {
  const { icon: Icon, typographyProps, iconProps, ...others } = props;
  const classes = useStyles();
  const isArray = Array.isArray(props.children);
  return (
    <Grid container className={classes.root} {...others}>
      <Grid item xs={1} className={classes.iconContainer}>
        <Icon className={classes.icon} {...iconProps} />
      </Grid>
      <Grid
        item
        xs={11}
        className={clsx(classes.textContainer, {
          [classes.multiLine]: isArray,
        })}
      >
        {isArray ? (
          props.children.map(v => (
            <Typography className={classes.text} {...typographyProps}>
              {v}
            </Typography>
          ))
        ) : (
          <Typography {...typographyProps}>{props.children}</Typography>
        )}
      </Grid>
    </Grid>
  );
}

RowInfoItem.propTypes = {
  icon: PropTypes.any,
  typographyProps: PropTypes.object,
  iconProps: PropTypes.object,
  children: PropTypes.any,
};

export default RowInfoItem;
