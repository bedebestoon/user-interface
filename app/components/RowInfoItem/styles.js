/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {},
  iconContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    margin: theme.spacing(1),
  },
  textContainer: {
    display: 'flex',
    padding: theme.spacing(1)
  },
  multiLine: {
    flexDirection: 'column'
  }
}));
