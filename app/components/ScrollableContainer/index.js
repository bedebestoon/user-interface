/**
 *
 * ScrollableContainer
 *
 */

import React from 'react';
import clsx from 'clsx';
import SimpleBar from 'simplebar-react';
import PropTypes from 'prop-types';

import useStyles from './styles';
import 'simplebar/dist/simplebar.min.css';

function ScrollableContainer(props) {
  const {
    className,
    style,
    location,
    match,
    history,
    staticContext,
    hidden,
    forwardRef,
    scrollableProps,
    ...others
  } = props;

  const classes = useStyles();

  return (
    <SimpleBar
      className={clsx(classes.root, { hidden }, className)}
      style={{
        ...style,
      }}
      {...others}
      scrollableNodeProps={{
        ...scrollableProps,
        ref: forwardRef,
        className: clsx(
          classes.scrollable,
          scrollableProps && scrollableProps.className,
        ),
        style: {
          overflowY: 'hidden',
        },
      }}
    />
  );
}

ScrollableContainer.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  location: PropTypes.any,
  match: PropTypes.any,
  history: PropTypes.any,
  staticContext: PropTypes.any,
  hidden: PropTypes.bool,
  forwardRef: PropTypes.any,
  scrollableProps: PropTypes.object,
};

export default ScrollableContainer;
