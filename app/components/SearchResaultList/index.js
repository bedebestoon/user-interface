/**
 *
 * SearchResaultList
 *
 */

import React, { memo, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';

import SearchItem from '../SearchResaultListItem';

import useStyles from './styles';

// import MOCK_DATA from './mock/searchResaultList.json';

function SearchResaultList(props) {
  const {
    state: {
      search: { data: searchData },
    },
  } = props;
  const [list, setList] = useState([]);
  const ref = useRef();

  useEffect(() => {
    if (searchData && searchData.length) return setList(searchData);
    setList([]);
  }, [searchData]);

  useEffect(() => {
    if (list.length) {
      ref.current.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }, [list]);

  const classes = useStyles();

  return (
    <div className={classes.root} ref={ref}>
      {list.map((item, i) => (
        // eslint-disable-next-line no-underscore-dangle
        <SearchItem key={item._id || i} {...item} isMobile={false} />
      ))}
    </div>
  );
}

SearchResaultList.propTypes = {
  state: PropTypes.object,
};

SearchResaultList.defaultProps = {
  state: {
    search: {
      data: [],
    },
  },
};

export default memo(SearchResaultList);
