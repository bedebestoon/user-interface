/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing((2 * 8) + 2),
    width: '100%',
    padding: `${theme.spacing(1)}px 0`,
    minHeight: '100vh',
  }
}));