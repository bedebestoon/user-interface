/**
 *
 * SearchItem
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import SwapVertIcon from '@material-ui/icons/SwapVert';

import useStyle from './styles';

function SearchItem(props) {
  const { giving, receiving, _id, isMobile, history } = props;

  const classes = useStyle();

  function handleInstruction() {
    history.push(`?item=${_id}`);
  }
  return (
    <Grid
      container
      onClick={handleInstruction}
      className={classes.root}
      component="button"
    >
      <Grid item xs md={5}>
        <Typography>
          {giving.fullName || giving.category.normalizedName}
        </Typography>
      </Grid>
      <Grid item xs md={2}>
        {isMobile ? <SwapVertIcon /> : <SwapHorizIcon />}
      </Grid>
      <Grid item xs md={5}>
        <Typography>
          {receiving.fullName || receiving.category.normalizedName}
        </Typography>
      </Grid>
    </Grid>
  );
}

SearchItem.propTypes = {
  giving: PropTypes.object,
  receiving: PropTypes.object,
  _id: PropTypes.string,
  isMobile: PropTypes.bool,
  history: PropTypes.any,
};

export default withRouter(memo(SearchItem));
