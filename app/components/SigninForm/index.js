/**
 *
 * SigninForm
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { useForm } from 'react-hook-form';

import { Link } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import useStyles from './styles';

function SigninForm(props) {
  const { onSubmit } = props;
  const classes = useStyles();

  const { register, handleSubmit } = useForm();

  function onSubmitForm(data) {
    if (onSubmit) {
      onSubmit(data);
    }
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          <FormattedMessage {...messages.header} />
        </Typography>
        <form
          className={classes.form}
          noValidate
          onSubmit={(e => e.preventDefault(), handleSubmit(onSubmitForm))}
        >
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label={messages.email.defaultMessage}
            name="username"
            autoComplete="email"
            autoFocus
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label={messages.password.defaultMessage}
            type="password"
            id="password"
            autoComplete="current-password-bedebestoon"
            inputRef={register}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label={messages.rememberMe.defaultMessage}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            <FormattedMessage {...messages.submit} />
          </Button>
          <Grid container>
            <Grid item>
              <Link to="/?signup=true" variant="body2">
                <FormattedMessage {...messages.signup} />
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>{/* <Copyright /> */}</Box>
    </Container>
  );
}

SigninForm.propTypes = {
  onSubmit: PropTypes.func,
};

export default memo(SigninForm);
