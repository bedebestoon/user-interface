/*
 * SigninForm Messages
 *
 * This contains all the text for the SigninForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.SigninForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'ورود به سایت',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'پست الکترونیک یا شماره تلفن',
  },
  password: {
    id: `${scope}.password`,
    defaultMessage: 'گذرواژه',
  },
  rememberMe: {
    id: `${scope}.rememberMe`,
    defaultMessage: 'به یاد بسپار.',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'ورود',
  },
  signup: {
    id: `${scope}.signup`,
    defaultMessage: 'حساب کاربری ندارید؟ همین حالا ثبت نام کنید.',
  },
});
