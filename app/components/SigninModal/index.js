/**
 *
 * TradeForm
 *
 */

import React, { memo, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Modal from '../Modal';
import SigninForm from '../SigninForm';
import SignupForm from '../SignupForm';

import useQueryString from '../../hooks/useQueryString';
import useStatusListener from '../../hooks/useStatusListener';

function SigninModal(props) {
  const {
    location,
    history,
    actions: { handleSignin },
    state: {
      user: { status: userSigninStatus },
    },
  } = props;
  const [open, setOpen] = useState(false);

  const query = useQueryString(location.search);

  useEffect(() => {
    if (query.signin) {
      setOpen('signin');
    } else if (query.signup) {
      setOpen('signup');
    } else {
      setOpen(false);
    }
  }, [query]);

  function handleCloseModal() {
    history.push(location.pathName);
  }

  useStatusListener(userSigninStatus, handleCloseModal)

  return (
    <>
      <Modal open={Boolean(open === 'signin')} onClose={handleCloseModal}>
        <SigninForm onSubmit={handleSignin} />
      </Modal>
      <Modal open={Boolean(open === 'signup')} onClose={handleCloseModal}>
        <SignupForm onSignup={handleSignin} />
      </Modal>
    </>
  );
}

SigninModal.propTypes = {
  location: PropTypes.any,
  history: PropTypes.any,
  actions: PropTypes.object,
  state: PropTypes.object,
};

export default withRouter(memo(SigninModal));
