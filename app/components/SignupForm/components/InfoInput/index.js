/**
 *
 * InfoInput
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import { useForm } from 'react-hook-form';

import TextField from 'components/DefaultTextField';
import Button from 'components/DefaultButton';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import useStyles from './styles';

function InfoInput(props) {
  const { onSubmit } = props;
  const { register, handleSubmit } = useForm();

  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          <FormattedMessage {...messages.header} />
        </Typography>
        <form
          className={classes.form}
          noValidate
          onSubmit={
            (e => e.preventDefault(), onSubmit && handleSubmit(onSubmit))
          }
        >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label={<FormattedMessage {...messages.firstName} />}
                autoFocus
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label={<FormattedMessage {...messages.lastName} />}
                name="lastName"
                autoComplete="lname"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={<FormattedMessage {...messages.email} />}
                name="email"
                autoComplete="email"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={<FormattedMessage {...messages.password} />}
                type="password"
                id="password"
                autoComplete="current-password"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                name="province"
                variant="outlined"
                required
                fullWidth
                id="province"
                label={<FormattedMessage {...messages.province} />}
                autoFocus
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="city"
                label={<FormattedMessage {...messages.city} />}
                name="city"
                inputRef={register}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            <FormattedMessage {...messages.submit} />
          </Button>
          <Grid container>
            <Grid item className={classes.link}>
              <Link to="/?signin=true" variant="body2">
                <FormattedMessage {...messages.signin} />
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>{/* <Copyright /> */}</Box>
    </Container>
  );
}

InfoInput.propTypes = {};

export default memo(InfoInput);
