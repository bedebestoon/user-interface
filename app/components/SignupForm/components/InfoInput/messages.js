/*
 * InfoInput Messages
 *
 * This contains all the text for the InfoInput component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.InfoInput';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'تکمیل پروفایل کاربری',
  },
  firstName: {
    id: `${scope}.firstName`,
    defaultMessage: 'نام',
  },
  lastName: {
    id: `${scope}.lastName`,
    defaultMessage: 'نام خانوادگی',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'پست الکترونیک',
  },
  password: {
    id: `${scope}.password`,
    defaultMessage: 'گذرواژه',
  },
  province: {
    id: `${scope}.province`,
    defaultMessage: 'استان',
  },
  city: {
    id: `${scope}.city`,
    defaultMessage: 'شهر',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'تایید',
  },
  signin: {
    id: `${scope}.signin`,
    defaultMessage: 'حساب کاربری دارید؟‌ وارد شوید.',
  },
});
