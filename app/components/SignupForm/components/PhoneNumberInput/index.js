/**
 *
 * PhoneNumberInput
 *
 */

import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import PhoneIcon from '@material-ui/icons/Phone';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import TextField from '../../../DefaultTextField';
import Button from '../../../DefaultButton';
import Title from '../../../Title';

import useStatusListener from '../../../../hooks/useStatusListener';

import useStyles from './styles';

function PhoneNumberInput(props) {
  const { onSubmit, status, next, onReject } = props;
  const [validNumber, setValidNumber] = useState('');
  const classes = useStyles();
  const ref = useRef();

  const handleChangeValue = e => {
    const phoneNumber = e.target.value.replace(/[^0-9]/gi, '');
    setValidNumber(phoneNumber);
  };

  function handleSubmit() {
    if (onSubmit) onSubmit(validNumber);
  }

  function handleSuccess() {
    if (next) next();
  }

  function handleReject() {
    if (onReject) onReject();
  }

  useStatusListener(status, handleSuccess, handleReject, true);

  return (
    <Grid container justify="center" className={classes.root} spacing={1}>
      <Title>
        <FormattedMessage {...messages.header} />
      </Title>
      <Grid item xs={11}>
        <TextField
          value={validNumber}
          onChange={handleChangeValue}
          className={classes.textField}
          startAdornment={<PhoneIcon />}
          label={<FormattedMessage {...messages.phoneInputLabel} />}
          placeholder={messages.placeholder.defaultMessage}
          autoFocus
          fullWidth
          inputRef={ref}
          inputProps={{
            maxLength: 11,
          }}
        />
      </Grid>
      <Grid item xs={11} className={classes.button}>
        <Button fullWidth onClick={handleSubmit}>
          {<FormattedMessage {...messages.submit} />}
        </Button>
      </Grid>
    </Grid>
  );
}

PhoneNumberInput.propTypes = {
  onSubmit: PropTypes.func,
  status: PropTypes.string,
  next: PropTypes.func,
  onReject: PropTypes.func,
};

export default PhoneNumberInput;
