/*
 * PhoneNumberInput Messages
 *
 * This contains all the text for the PhoneNumberInput component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.PhoneNumberInput';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'لطفا شماره تلفن همراه خود را وارد کنید',
  },
  phoneInputLabel: {
    id: `${scope}.phoneInputLabel`,
    defaultMessage: 'شماره تلفن',
  },
  placeholder: {
    id: `${scope}.placeholder`,
    defaultMessage: '09XXXXXXXX',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'تایید',
  },
});
