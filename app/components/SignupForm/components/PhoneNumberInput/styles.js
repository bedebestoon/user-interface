/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    width: '100%',
    alignItems: 'center',
    padding: theme.spacing(1)
  },
  textField:{
    direction: 'rtl'
  }
}));
