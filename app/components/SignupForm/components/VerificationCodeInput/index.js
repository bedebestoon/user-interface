/**
 *
 * SignupForm
 *
 */

import React, {
  memo,
  useState,
  useRef,
  useEffect,
  // useImperativeHandle,
  // forwardRef,
} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import CustomInput from '../../../DefaultTextField';
import Title from '../../../Title';
import Button from '../../../DefaultButton';

import useStyles from './styles';

function SignupForm(props) {
  const { regex = /^[0-9]+$/, onChange, onSubmit, status, next } = props;

  const initialDigits = ['', '', '', ''];
  const [digits, setDigits] = useState(initialDigits);

  const inputRefs = [useRef(null), useRef(null), useRef(null), useRef(null)];

  useEffect(() => {
    if (inputRefs[0].current) {
      inputRefs[0].current.focus();
    }
  }, []);

  function digitChange(event, index) {
    const { value } = event.target;

    if (!regex.test(value)) {
      return;
    }

    const newDigits = [...digits];
    newDigits[index] = getNewChar(digits[index], value);
    change(newDigits);

    if (index < inputRefs.length - 1) {
      inputRefs[index + 1].current.focus();
    }
  }

  function keyDown(event, index) {
    const keyCode = event.which || event.keyCode;
    if (keyCode === 8) {
      const newDigits = [...digits];
      newDigits[index] = '';
      change(newDigits);

      if (index > 0) {
        inputRefs[index - 1].current.focus();
      }
    }
  }

  function change(newDigits) {
    setDigits(newDigits);
    if (onChange) {
      onChange(newDigits);
    }
  }

  function handleSubmit() {
    if (onSubmit) onSubmit(digits.join(''));
  }

  const classes = useStyles();

  return (
    <Grid container className={clsx(classes.root)}>
      <Title>
        <FormattedMessage {...messages.header} />
      </Title>
      <div className={classes.inputsContainer}>
        <CustomInput
          inputRef={inputRefs[0]}
          className={classes.textField}
          variant="outlined"
          inputProps={{
            className: classes.input,
            type: 'tel',
            placeholder: 'x',
            value: digits[0],
            onChange: event => digitChange(event, 0),
            onKeyDown: event => keyDown(event, 0),
            // inputRef: inputRefs[0],
          }}
        />
        <CustomInput
          className={classes.textField}
          variant="outlined"
          inputRef={inputRefs[1]}
          inputProps={{
            className: classes.input,
            type: 'tel',
            placeholder: 'x',
            value: digits[1],
            onChange: event => digitChange(event, 1),
            onKeyDown: event => keyDown(event, 1),
            // inputRef: inputRefs[1],
          }}
        />
        <CustomInput
          className={classes.textField}
          variant="outlined"
          inputRef={inputRefs[2]}
          inputProps={{
            className: classes.input,
            type: 'tel',
            placeholder: 'x',
            value: digits[2],
            onChange: event => digitChange(event, 2),
            onKeyDown: event => keyDown(event, 2),
            // inputRef: inputRefs[2],
          }}
        />
        <CustomInput
          className={classes.textField}
          variant="outlined"
          inputRef={inputRefs[3]}
          inputProps={{
            className: classes.input,
            type: 'tel',
            placeholder: 'x',
            value: digits[3],
            onChange: event => digitChange(event, 3),
            onKeyDown: event => keyDown(event, 3),
            // inputRef: inputRefs[3],
          }}
        />
      </div>
      <Grid item xs={11} className={classes.button}>
        <Button fullWidth onClick={handleSubmit}>
          {<FormattedMessage {...messages.submit} />}
        </Button>
      </Grid>
    </Grid>
  );
}

function getNewChar(prevStr, newStr) {
  if (prevStr === newStr[0]) {
    return newStr[1];
  }
  return newStr[0];
}

SignupForm.propTypes = {
  onSubmit: PropTypes.func,
  status: PropTypes.string,
  next: PropTypes.func,
  regex: PropTypes.any,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

export default memo(SignupForm);
