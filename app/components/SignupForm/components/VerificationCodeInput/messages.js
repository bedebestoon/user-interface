/*
 * VerificationCodeInput Messages
 *
 * This contains all the text for the VerificationCodeInput component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.VerificationCodeInput';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'لطفا کد ارسال شده را وارد کنید',
  },
  submit: {
    id: `${scope}.submit`,
    defaultMessage: 'تایید',
  }
});
