/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  textField: {
    margin: theme.spacing(1),
    '& div': {
      padding: 0,
    }
  },
  inputsContainer: {
    direction: 'rtl',
  },
  input: {
    textAlign: 'center',
    width: 30,

    '& input': {
      textAlign: 'center',
      fontSize: theme.typography.body1.fontSize
    }
  }
}));
