/* eslint-disable prettier/prettier */
import { useState } from 'react';

import apiHandler from '../../../../utils/apiHandler';
import {
  GET_USER_BY_PHONE_NUMBER_API,
  GET_USER_VERIFICATION_CODE_API,
  VERIFY_CODE_API,
  SIGNUP_API,
} from '../../../../config/api';

export default function () {
  const [phoneNumber, setPhoneNumber] = useState();

  function getUserByPhoneNumber(_phoneNumber) {
    setPhoneNumber(_phoneNumber);
    return apiHandler(GET_USER_BY_PHONE_NUMBER_API, { phoneNumber: _phoneNumber })
  }

  function getVerificationCode(_phoneNumber) {
    return apiHandler(GET_USER_VERIFICATION_CODE_API, { phoneNumber: _phoneNumber })
  }

  function verifyCode(code) {
    if (!phoneNumber || !code)
      return false;

    return apiHandler(VERIFY_CODE_API, {
      phoneNumber,
      code
    })
  }

  function registerUser(user) {
    if (!user)
      return false;
    console.log(phoneNumber)
    return apiHandler(SIGNUP_API, { ...user, phoneNumber });
  }

  return {
    getUserByPhoneNumber,
    getVerificationCode,
    verifyCode,
    registerUser,
  };
}
