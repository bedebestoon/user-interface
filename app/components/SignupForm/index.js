/**
 *
 * SignupForm
 *
 */

import React, { memo, useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import SwipeableViews from 'react-swipeable-views';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

import PhoneNumberInput from './components/PhoneNumberInput';
import VerificationCodeInput from './components/VerificationCodeInput';
import InfoInput from './components/InfoInput';

import useHandleUser from './hooks/useHandleUser';

import useStyles from './styles';

const initialOptions = {
  error: null,
  isDuplicate: false,
};

function SignupForm(props) {
  const { onSignup } = props;

  const [options, setOptions] = useState(initialOptions);

  const {
    getUserByPhoneNumber,
    getVerificationCode,
    verifyCode,
    registerUser,
  } = useHandleUser();

  const [step, setStep] = useState(0);
  const classes = useStyles();

  const ref = useRef();

  useEffect(() => {
    handleUpdateHeight();
  }, []);

  function handleOptions(prop, value) {
    setOptions(prev => ({ ...prev, [prop]: value }));
  }

  function View(props) {
    // eslint-disable-next-line react/prop-types
    return <div className={classes.view}>{props.children}</div>;
  }

  function handleUpdateHeight() {
    if (ref.current) {
      ref.current.updateHeight();
    }
  }

  function nextStep() {
    handleOptions('error', null);
    setStep(s => s + 1);
  }

  function handleSubmitPhone(v) {
    getUserByPhoneNumber(v)
      .then(() => handleOptions('isDuplicate', true))
      .catch(() => {
        getVerificationCode(v)
          .then(() => nextStep())
          .catch(e => handleOptions('error', e));
      });
  }

  function handleSubmitCode(v) {
    verifyCode(v)
      .then(() => nextStep())
      .catch(e => handleOptions('error', e));
  }

  function handleSubmitInfo(v) {
    const registerProps = {
      ...v,
      place: {
        province: v.province,
        city: v.city,
      }
    };
    registerUser(registerProps)
      .then(() => {
        if (onSignup)
          onSignup({ phoneNumber: v.phoneNumber, password: v.password });
      })
      .catch(e => handleOptions('error', e));
  }

  return (
    <div className={classes.root}>
      <SwipeableViews
        className={classes.swipeableViews}
        index={step}
        onChangeIndex={handleUpdateHeight}
        // animateHeight
        // action={c => c.updateHeight()}
        ref={ref}
      >
        <View>
          <PhoneNumberInput
            onSubmit={handleSubmitPhone}
            isDuplicate={options.isDuplicate}
          />
        </View>
        <View>
          <VerificationCodeInput
            onSubmit={handleSubmitCode}
            error={options.error}
          />
        </View>
        <View>
          <InfoInput onSubmit={handleSubmitInfo} error={options.error} />
        </View>
      </SwipeableViews>
    </div>
  );
}

SignupForm.propTypes = {
  onSignup: PropTypes.func,
};

export default memo(SignupForm);
