/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    
  },
  swipeableViews: {

  },
  view: {
    padding: theme.spacing(3),
    height: '100%',
    direction: 'ltr !important'
  }
}));
