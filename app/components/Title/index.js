/**
 *
 * Title
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import useStyles from './styles';

function Title(props) {
  const {
    align,
    color,
    variant,
    component,
    gutterBottom,
    divider,
    icon: Icon,
    iconComponent: IconComponent,
    className,
    children,
  } = props;

  const classes = useStyles();
  return (
    <>
      <Grid
        container
        item
        xs={12}
        justify="space-between"
        // direction="row-reverse"
      >
        <Typography
          component={component}
          variant={variant}
          className={clsx(classes.root, className)}
          align={align}
          color={color}
          gutterBottom={gutterBottom}
        >
          {children}
        </Typography>
        {Icon && <Icon color={color} />}
        {IconComponent && <IconComponent />}
      </Grid>
      {divider && <Divider className={classes.divider} />}
    </>
  );
}

Title.propTypes = {
  children: PropTypes.any,
  align: PropTypes.string,
  divider: PropTypes.bool,
  color: PropTypes.string,
  variant: PropTypes.string,
  component: PropTypes.string,
  gutterBottom: PropTypes.bool,
  icon: PropTypes.any,
  iconComponent: PropTypes.func,
  className: PropTypes.any,
};

Title.defaultProps = {
  align: 'right',
  color: 'secondary',
  variant: 'h6',
  component: 'h2',
  gutterBottom: true,
  divider: false,
};

export default Title;
