/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    padding: `0 ${theme.spacing(2)}px`,
  },
  divider: {
    width: '100%',
    background: `linear-gradient(145deg, rgba(255,255,255,1) 0%, ${theme.palette.primary.main} 50%)`
  }
}));
