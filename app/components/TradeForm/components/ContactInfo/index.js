/**
 *
 * ContactInfo
 *
 */

import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import Button from '../../../DefaultButton';
import RowInfoItem from '../../../RowInfoItem';
import { getIconSrc } from '../../../Icons';

import useStyles from './styles';

function ContactInfo(props) {
  const { data } = props;
  const [show, setShow] = useState(false);
  const classes = useStyles();

  function ShowContacts() {
    const displayArray = [];
    Object.keys(data).forEach((key, i) => {
      const Icon = getIconSrc(key);
      displayArray.push(
        <RowInfoItem key={i} icon={Icon}>
          {data[key]}
        </RowInfoItem>,
      );
    });
    return displayArray;
  }

  function HideContacts() {
    return (
      <Grid container className={classes.hiddenContainer}>
        <Grid item xs={10} md={4}>
          <Button color="secondary" onClick={() => setShow(true)} fullWidth>
            <FormattedMessage {...messages.showContact} />
          </Button>
        </Grid>
      </Grid>
    );
  }

  return (
    <div className={classes.root}>
      {show ? <ShowContacts /> : <HideContacts />}
    </div>
  );
}

ContactInfo.propTypes = {
  data: PropTypes.any,
};

export default ContactInfo;
