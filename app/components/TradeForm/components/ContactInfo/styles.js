/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  root: {
    width: '100%',
    minHeight: 100,
  },
  hiddenContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  }
}));
