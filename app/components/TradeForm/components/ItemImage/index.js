/**
 *
 * ItemImage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import ImageCarousel from '../../../ImageCarousel';

import useStyles from './styles';

function ItemImage(props) {
  const { data } = props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <ImageCarousel data={data} />
    </div>
  );
}

ItemImage.propTypes = {
  data: PropTypes.any,
};

export default ItemImage;
