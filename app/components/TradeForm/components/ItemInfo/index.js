/**
 *
 * ItemInfo
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';

// import AccessTimeIcon from '@material-ui/icons/AccessTime';
import DescriptionIcon from '@material-ui/icons/Description';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

// import addWorkingDurationText from '../../../../functions/addWorkingDurationText';

import Title from '../../../Title';
import RowInfoItem from '../../../RowInfoItem';
import { getIconSrc } from '../../../Icons';

import useStyles from './styles';

function ItemInfo(props) {
  const { data } = props;
  const { giving, receiving } = data;
  const classes = useStyles();

  const CategoryIcon = getIconSrc(giving.category.name);

  // const transformedWorkingDuration = addWorkingDurationText(workingDuration);

  // function handleWorkingDurationText() {
  //   if (transformedWorkingDuration.isNew)
  //     return transformedWorkingDuration.text;
  //   return transformedWorkingDuration.text + messages.workedFor.defaultMessage;
  // }

  return (
    <Grid container className={classes.root}>
      <Title divider>
        <FormattedMessage {...messages.title} />
      </Title>
      <RowInfoItem icon={CategoryIcon}>{giving.fullName}</RowInfoItem>
      {giving.desc && (
        <RowInfoItem icon={DescriptionIcon}>{giving.desc}</RowInfoItem>
      )}
    </Grid>
  );
}

ItemInfo.propTypes = {
  data: PropTypes.any,
};

ItemInfo.defaultProps = {
  data: {
    giving: {},
  },
};

export default ItemInfo;
