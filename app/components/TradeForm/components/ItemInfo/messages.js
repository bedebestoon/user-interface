/*
 * ItemInfo Messages
 *
 * This contains all the text for the ItemInfo component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ItemInfo';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'مشخصات کالا',
  },
  workedFor: {
    id: `${scope}.workedFor`,
    defaultMessage: ' کار کرده.',
  },
});
