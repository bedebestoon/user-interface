/**
 *
 * ItemInfo
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';

import PersonIcon from '@material-ui/icons/Person';
import PinDropIcon from '@material-ui/icons/PinDrop';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import Title from '../../../Title';
import RowInfoItem from '../../../RowInfoItem';

import ContactInfo from '../ContactInfo';

import useStyles from './styles';

function TraderInfo(props) {
  const { data } = props;
  const { firstName, lastName, email, phoneNumber, place = {} } = data;
  const classes = useStyles();

  const contactInfoProps = {
    email,
    phoneNumber,
  };

  return (
    <Grid container className={classes.root}>
      <Title divider>
        <FormattedMessage {...messages.title} />
      </Title>
      <RowInfoItem icon={PersonIcon}>{`${firstName} ${lastName}`}</RowInfoItem>
      <RowInfoItem icon={PinDropIcon}>
        {`${place.province || ''} / ${place.city || ''}`}
      </RowInfoItem>
      <ContactInfo data={contactInfoProps} />
    </Grid>
  );
}

TraderInfo.propTypes = {
  data: PropTypes.any,
};

export default TraderInfo;
