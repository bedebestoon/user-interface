/*
 * TraderInfo Messages
 *
 * This contains all the text for the TraderInfo component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.TraderInfo';

export default defineMessages({
  title: {
    id: `${scope}.title`,
    defaultMessage: 'مشخصات معامله گر',
  },
});
