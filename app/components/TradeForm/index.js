/**
 *
 * TradeForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import ItemImage from './components/ItemImage';
import ItemInfo from './components/ItemInfo';
import TraderInfo from './components/TraderInfo';

function TradeForm(props) {
  const { item = {}, container } = props;
  const itemInfo = {
    giving: item.giving || {},
    receiving: item.receiving || {},
  };
  return (
    <div ref={container}>
      <ItemImage data={item.images} />
      <ItemInfo data={itemInfo} />
      <TraderInfo data={item.owner} />
    </div>
  );
}

TradeForm.propTypes = {
  container: PropTypes.any,
  item: PropTypes.oneOfType([
    PropTypes.shape({
      images: PropTypes.array,
      itemInfo: PropTypes.object,
      traderInfo: PropTypes.object,
    }),
    PropTypes.bool,
  ]),
};

TradeForm.defaultProps = {
  item: {
    giving: {
      category: '',
      fullName: '',
      desc: '',
    },
    recieving: {
      category: '',
      fullName: '',
      desc: '',
    },
    images: [],
    owner: {
      firstName: '',
      lastName: '',
      place: {
        city: '',
        province: '',
      },
      phoneNumber: '',
      email: '',
    },
  },
};

export default TradeForm;
