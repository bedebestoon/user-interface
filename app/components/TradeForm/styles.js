/* eslint-disable prettier/prettier */
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    margin: `${theme.spacing(2)}px 0`,
    padding: theme.spacing(1),
    borderRadius: theme.shape.borderRadius,
    border: `1px ${theme.palette.grey[300]} solid`,
    '&::hover':{
      pointer: 'cursor !mportant'
    },
    '& div': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }
  }
}));