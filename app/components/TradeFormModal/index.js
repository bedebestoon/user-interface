/**
 *
 * TradeForm
 *
 */

import React, { memo, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Modal from '../Modal';
import TradeForm from '../TradeForm';

import useQueryString from '../../hooks/useQueryString';

function TradeFormModal(props) {
  const {
    location,
    history,
    state: {
      search: { data: searchData = [] },
    },
  } = props;

  const [item, setItem] = useState(null);
  const [open, setOpen] = useState(false);

  const query = useQueryString(location.search);

  useEffect(() => {
    if (query.item) {
      setItem(null);
      // eslint-disable-next-line no-underscore-dangle
      const foundItem = searchData.find(o => o._id === query.item);
      if (foundItem) {
        setItem(foundItem);
      }
    }
  }, [query.item]);

  useEffect(() => {
    if (item) {
      setOpen(true);
    }
  }, [item]);

  function handleCloseModal() {
    setOpen(false);
    history.push(location.pathName);
  }

  return (
    <Modal open={open && item} onClose={handleCloseModal}>
      <TradeForm item={item} />
    </Modal>
  );
}

TradeFormModal.propTypes = {
  location: PropTypes.any,
  history: PropTypes.any,
  state: PropTypes.object,
};

export default withRouter(memo(TradeFormModal));
