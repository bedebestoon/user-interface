/* eslint-disable prettier/prettier */
export const API_ADDRESS = 'http://localhost:3010';
export const GET_USER_BY_PHONE_NUMBER_API=  {
  method: 'post',
  url: '/user/getUserByPhoneNumber',
};
export const GET_USER_VERIFICATION_CODE_API=  {
  method: 'post',
  url: '/verification/getVerificationCode',
};
export const VERIFY_CODE_API=  {
  method: 'post',
  url: '/verification/verifyCode',
};
export const SIGNIN_API = {
  method: 'post',
  url: '/user/login',
};
export const SIGNUP_API = {
  method: 'post',
  url: '/user/register',
};
export const REGISTER_TRADE_API = {
  method: 'post',
  url: '/trade/registerTrade',
};
export const SEARCH_TRADES_API = {
  method: 'post',
  url: '/trade/searchTrades',
};
export const GET_TRADE_GROUPS_API = {
  method: 'get',
  url: '/category/getAllCategories',
};
export const UPLOAD_IMAGE_API = {
  method: 'post',
  url: '/upload/image',
};