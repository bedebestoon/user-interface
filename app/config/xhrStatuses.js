/* eslint-disable prettier/prettier */
export const XHR_FULFILLED = 'XHR_FULFILLED';
export const XHR_REJECTED = 'XHR_REJECTED';
export const XHR_PENDING = 'XHR_PENDING';
export const XHR_IDLE = 'XHR_IDLE';
