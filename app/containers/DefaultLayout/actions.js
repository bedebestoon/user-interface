/*
 *
 * DefaultLayout actions
 *
 */

import {
  SIGNIN,
  SIGNUP,
  SIGNOUT,
  REGISTER_TRADE,
  SEARCH_TRADES,
  GET_TRADE_GROUPS,
} from './constants';

export function signin(data) {
  return {
    type: SIGNIN,
    data,
  };
}

export function signup(data) {
  return {
    type: SIGNUP,
    data,
  };
}

export function signout(data) {
  return {
    type: SIGNOUT,
    data,
  };
}

export function registerTrade(data) {
  return {
    type: REGISTER_TRADE,
    data,
  };
}

export function searchTrades(data) {
  return {
    type: SEARCH_TRADES,
    data,
  };
}

export function getTradeGroups(data) {
  return {
    type: GET_TRADE_GROUPS,
    data,
  };
}