/*
 *
 * DefaultLayout constants
 *
 */

export const SIGNIN = 'SIGNIN';
export const SIGNUP = 'SIGNUP';
export const SIGNOUT = 'SIGNOUT';
export const SEARCH_TRADES = 'SEARCH_TRADES';
export const REGISTER_TRADE = 'REGISTER_TRADE';
export const GET_TRADE_GROUPS = 'GET_TRADE_GROUPS';
