/**
 *
 * DefaultLayout
 *
 */

import React, { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Container from '@material-ui/core/Container';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import {
  signin,
  signup,
  signout,
  searchTrades,
  registerTrade,
  getTradeGroups,
} from './actions';

import makeSelectDefaultLayout, { makeSelectUserData } from './selectors';
import reducer from './reducer';
import saga from './saga';

// import messages from './messages';
import useStyles from './styles';

import Navbar from '../../components/Navbar';

const DefaultLayout = forwardRef((props, ref) => {
  useInjectReducer({ key: 'defaultLayout', reducer });
  useInjectSaga({ key: 'defaultLayout', saga });

  const { user, actions } = props;

  const classes = useStyles();
  const isLoggedIn = Boolean(user);
  console.log(isLoggedIn, user)

  function handleLogin() {
    if (isLoggedIn) return actions.handleSignout();
    return props.history.push(`?signin=true`);
  }

  function handleRegisterTrade() {
    props.history.push(`?registerTrade=true`);
  }

  return (
    <>
      <Navbar
        onClickLogin={handleLogin}
        onClickRegisterTrade={handleRegisterTrade}
        isLoggedIn={isLoggedIn}
      />
      <Container maxWidth="md" className={classes.root} innerRef={ref}>
        {React.Children.map(props.children, child =>
          React.cloneElement(child, props, null),
        )}
      </Container>
    </>
  );
});

DefaultLayout.propTypes = {
  children: PropTypes.any,
  history: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  state: makeSelectDefaultLayout(),
  user: makeSelectUserData(),
});

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      handleRegisterTrade: props => dispatch(registerTrade(props)),
      handleSearchTrades: props => dispatch(searchTrades(props)),
      handleSignin: props => dispatch(signin(props)),
      handleSignup: props => dispatch(signup(props)),
      handleSignout: () => dispatch(signout()),
      handleGetTradeGroups: () => dispatch(getTradeGroups()),
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withRouter,
  withConnect,
)(DefaultLayout);
