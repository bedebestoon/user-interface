/*
 *
 * DefaultLayout reducer
 *
 */
import produce from 'immer';

import {
  SIGNIN,
  SIGNUP,
  SIGNOUT,
  REGISTER_TRADE,
  SEARCH_TRADES,
  GET_TRADE_GROUPS,
} from './constants';

import { reqIdlePayload } from '../../utils/apiHandler';

export const initialState = {
  user: reqIdlePayload(),
  search: reqIdlePayload(),
  signup: reqIdlePayload(),
  registerTrade: reqIdlePayload(),
  tradeGroups: reqIdlePayload(),
};

/* eslint-disable default-case, no-param-reassign */
const defaultLayoutReducer = (state = initialState, action) =>
  // eslint-disable-next-line consistent-return
  produce(state, draft => {
    const { type: _type, payload } = action;
    const type = _type.replace('_SagaRequestHandler', '');
    if (payload) {
      switch (type) {
        case SIGNIN:
          draft.user = payload;
          break;
        case SIGNUP:
          draft.signup = payload;
          break;
        case SIGNOUT:
          draft.user = initialState.user;
          break;
        case REGISTER_TRADE:
          draft.registerTrade = payload;
          break;
        case SEARCH_TRADES:
          draft.search = payload;
          break;
        case GET_TRADE_GROUPS:
          draft.tradeGroups = payload;
          break;
      }
    } else {
      switch (type) {
        case SIGNOUT:
          draft.user = initialState.user;
          break;
      }
    };
  });

export default defaultLayoutReducer;
