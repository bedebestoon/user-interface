import { takeLatest, all, takeEvery } from 'redux-saga/effects';
import sagaRequestHandler from '../../utils/sagaRequestHandler';

import {
  SIGNIN,
  SIGNUP,
  SEARCH_TRADES,
  REGISTER_TRADE,
  GET_TRADE_GROUPS,
} from './constants';

import {
  SIGNIN_API,
  SIGNUP_API,
  REGISTER_TRADE_API,
  SEARCH_TRADES_API,
  GET_TRADE_GROUPS_API,
} from '../../config/api';

// Individual exports for testing
function* signin(action) {
  yield sagaRequestHandler(SIGNIN, SIGNIN_API, action.data);
}

function* signinWatcher() {
  yield takeLatest(SIGNIN, signin);
}

function* signup(action) {
  yield sagaRequestHandler(SIGNUP, SIGNUP_API, action.data);
}

function* signupWatcher() {
  yield takeLatest(SIGNUP, signup);
}

function* registerTrade(action) {
  yield sagaRequestHandler(REGISTER_TRADE, REGISTER_TRADE_API, action.data);
}

function* registerTradeWatcher() {
  yield takeLatest(REGISTER_TRADE, registerTrade);
}

function* searchTrades(action) {
  yield sagaRequestHandler(SEARCH_TRADES, SEARCH_TRADES_API, action.data);
}

function* searchTradesWatcher() {
  yield takeLatest(SEARCH_TRADES, searchTrades);
}

function* getTradeGroups() {
  yield sagaRequestHandler(GET_TRADE_GROUPS, GET_TRADE_GROUPS_API);
}

function* getTradeGroupsWatcher() {
  yield takeEvery(GET_TRADE_GROUPS, getTradeGroups);
}

export default function* rootSaga() {
  yield all([
    getTradeGroupsWatcher(),
    searchTradesWatcher(),
    registerTradeWatcher(),
    signinWatcher(),
    signupWatcher(),
  ]);
}
