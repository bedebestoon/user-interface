import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the defaultLayout state domain
 */

const selectDefaultLayoutDomain = state => state.defaultLayout || initialState;

/**
 * Other specific selectors
 */

const selectUser = state => state.user.data;
/**
 * Default selector used by DefaultLayout
 */

const makeSelectDefaultLayout = () =>
  createSelector(
    selectDefaultLayoutDomain,
    substate => substate,
  );

const makeSelectUserData = () =>
  createSelector(
    selectDefaultLayoutDomain,
    substate => selectUser(substate),
  );

export default makeSelectDefaultLayout;
export { selectDefaultLayoutDomain, makeSelectUserData };
