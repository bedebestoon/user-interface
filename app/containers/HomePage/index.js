/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

import DefaultLayout from '../DefaultLayout';

import MainSearch from '../../components/MainSearch';
import SearchResultList from '../../components/SearchResaultList';
import TradeFormModal from '../../components/TradeFormModal';
import SigninModal from '../../components/SigninModal';
import RegisterTradeFormModal from '../../components/RegisterTradeFormModal';

export default function HomePage() {
  return (
    <DefaultLayout>
      <MainSearch />
      <SearchResultList />
      <TradeFormModal />
      <SigninModal />
      <RegisterTradeFormModal />
    </DefaultLayout>
  );
}
