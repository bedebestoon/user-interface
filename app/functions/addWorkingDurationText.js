/* eslint-disable prettier/prettier */
export default function (props) {
  const { value, period } = props;

  function getPeriod() {
    switch (period) {
      case 'new':
        return 'کارنکرده (نو)';
      case 'hour':
        return 'ساعت';
      case 'day':
        return 'روز';
      case 'month':
        return 'ماه';
      case 'year':
        return 'سال';
      default:
        return 'نا مشخص';
    }
  }

  const out = {
    value,
    period,
    text: '',
    isNew: false,
    isValid: true,
  }

  if (period && value) {
    if (period === 'new') {
      out.text = period;
      out.isNew = true;
    }
    out.text = `${value || ""} ${getPeriod()}`
  } else {
    out.text = 'کارد نامشخص';
    out.isValid = true;
  }

  return out
}
