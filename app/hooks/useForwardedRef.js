/* eslint-disable prettier/prettier */
import { useState, useRef, useEffect } from 'react';

export default function (forwardedRef) {
  const [ref, setRef] = useState(null);

  useEffect(() => {
    if (!forwardedRef) return;
    setRef(forwardedRef.current);
  }, [forwardedRef]);

  return ref;
}