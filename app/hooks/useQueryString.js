/* eslint-disable prettier/prettier */
import { useEffect, useState } from 'react';
import queryString from 'query-string';

export default function (search) {
  const [query, setQuery] = useState({})
  
  useEffect(() => {
    if (search) {
      setQuery(queryString.parse(search));
    } else {
      setQuery({});
    }
  }, [search]);

  return query
}