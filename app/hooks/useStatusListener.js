/* eslint-disable prettier/prettier */
import { useEffect, useState } from 'react';

import {
  XHR_PENDING,
  XHR_REJECTED,
  XHR_FULFILLED,
  XHR_IDLE,
} from '../config/xhrStatuses';

const DELAY_MS = 500;

export default function (_status, onSuccess, onReject, delay) {
  const [listen, setListen] = useState(false);
  const [status, setStatus] = useState(0);

  function delayedResponse(func) {
    let to
    let delayTime
    if (delay) {
      delayTime = delay;
      if (typeof delay === 'boolean') delayTime = DELAY_MS;
      to = setTimeout(func, delayTime);
      return () => clearTimeout(to);
    }
    return func();
  }

  useEffect(() => {
    if (_status === XHR_PENDING || _status === XHR_IDLE) {
      setListen(true);
      setStatus(_status)
    } else if (listen) {
      setListen(false);
      if (_status === XHR_FULFILLED) {
        setStatus(_status);
        if (onSuccess) {
          delayedResponse(onSuccess);
        };
      } else if (_status === XHR_REJECTED) {
        setStatus(_status);
        if (onReject) {
          delayedResponse(onReject);
        };
      }
    } else {
      setStatus(XHR_IDLE)
    }
  }, [_status]);

  const isLoading = status === XHR_PENDING;
  const isFulfilled = status === XHR_FULFILLED;
  const isRejected = status === XHR_REJECTED;

  return {
    status,
    isLoading,
    isFulfilled,
    isRejected,
  };
}