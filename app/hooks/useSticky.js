/* eslint-disable prettier/prettier */
import { useEffect, useState, useRef } from 'react';

function useSticky(trigger) {
  const [isSticky, setSticky] = useState(false);
  const element = useRef(null);

  function handleScroll() {
    const finalTrigger = trigger || (element.current ? element.current.getBoundingClientRect().bottom : Infinity);
    setSticky(
      window.scrollY > (finalTrigger)
    )
  };
  // This function handle the scroll performance issue
  const debounce = (func, wait = 0, immediate = true, ...rest) => {
    let timeOut
    return () => {
      const context = this;
      const args = rest;
      const later = () => {
        timeOut = null
        if (!immediate) func.apply(context, args)
      }
      const callNow = immediate && !timeOut
      clearTimeout(timeOut)
      timeOut = setTimeout(later, wait)
      if (callNow) func.apply(context, args)
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)
    return () => {
      window.removeEventListener("scroll", () => handleScroll)
    }
  }, [debounce, handleScroll])

  return { isSticky, element }
}

export default useSticky