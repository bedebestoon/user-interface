/* eslint-disable prettier/prettier */
import { createMuiTheme } from '@material-ui/core/styles'

import iransansWoff from './fonts/iransans.woff';

const iransans = {
  fontFamily: 'iransans',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('iransans'),
    local('iransans'),
    url(${iransansWoff}) format('woff')
  `,
};

const theme = createMuiTheme({
  direction: 'rtl',
  typography: {
    fontFamily: 'iransans, Roboto, Arial',
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [iransans],
      },
    },
  },
  props: {
    MuiTextField: {
      margin: 'dense',
      size: 'small'
    }
  }
});

export default theme