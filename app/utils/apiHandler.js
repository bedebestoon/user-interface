/* eslint-disable prettier/prettier */
import Axios from "axios";
import { API_ADDRESS } from "../config/api";

import {
  XHR_PENDING,
  XHR_FULFILLED,
  XHR_REJECTED,
  XHR_IDLE,
} from '../config/xhrStatuses';

const instance = Axios.create({
  baseURL: API_ADDRESS,
});

instance.interceptors.request.use(
  config => {
    if (localStorage.getItem('token')) {
      config.headers["x-access-token"] = localStorage.getItem('token');
    }
    return config;
  }, error => Promise.reject(error)
);

instance.interceptors.response.use(response => response.data, error => {
  let errorBody
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    errorBody = ({
      data: error.response.data,
      status: error.response.status,
    })
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    errorBody = ({
      data: error.request
    })
  } else {
    // Something happened in setting up the request that triggered an Error
    errorBody = ({
      data: error.message
    })
  }
  return Promise.reject(errorBody)
})

export default (config, data) => instance({ ...config, data })

export const reqIdlePayload = () => ({
  status: XHR_IDLE,
  data: null,
  error: null,
  isLoading: false,
})

export const reqPendingPayload = () => ({
  status: XHR_PENDING,
  data: null,
  error: null,
  isLoading: true,
})

export const reqFulfilledPayload = (res) => ({
  status: XHR_FULFILLED,
  data: res,
  error: null,
  isLoading: false,
})

export const reqRejectedPayload = (err) => ({
  status: XHR_REJECTED,
  data: null,
  error: err.data,
  isLoading: false,
})