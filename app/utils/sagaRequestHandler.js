/* eslint-disable prettier/prettier */

import { call, put } from 'redux-saga/effects';

import apiHandler, {
  reqPendingPayload,
  reqFulfilledPayload,
  reqRejectedPayload,
} from './apiHandler';

export default function* (type, reqOptions, data = {}) {
  // console.log('>>>', type, reqOptions, data)
  const suffix = '_SagaRequestHandler'
  const typeName = type + suffix
  try {
    yield put({ type: typeName, payload: reqPendingPayload() })
    const res = yield call(apiHandler, reqOptions, data)
    yield put({ type: typeName, payload: reqFulfilledPayload(res) })
  } catch (e) {
    yield put({ type: typeName, payload: reqRejectedPayload(e) });
  }
}
